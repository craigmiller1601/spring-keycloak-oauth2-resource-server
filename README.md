# spring-keycloak-oauth2-resource-server

A wrapper around the necessary code to setup a Spring Boot OAuth2 Resource Server with my Keycloak Authorization Server.

## Pre-Requisites

This library requires Spring Boot 3.4.1 or higher. While it is compatible with Java, it also requires the Kotlin Stdlib 2.1.0 or higher.

Lastly, if used with Spring Webflux, it requires Kotlin Coroutines 1.10.1 or higher.

## How to Use

The library uses spring auto-configuration to setup the consuming project. However, there are some requirements still:

First, setup the following properties to configure the resource server:

```yaml
spring:
  security:
    keycloak:
      oauth2:
        resourceserver:
          host: ###
          realm: ###
          client-id: ###
          # Optional if only interested in receiving requests, required to generate client
          client-secret: ###
          # Optional if there are alternate hostnames to trust as an issuer
          alt-hosts:
            - ###
            - ###
```

Then, update your web security config accordingly:

```kotlin
class WebSecurityConfig(
    private val keycloakOAuth2ResourceServerProvider: KeycloakOAuth2ResourceServerProvider
) {
    // For WebMVC
    @Bean
    fun securityFilterChain(http: HttpSecurity): DefaultSecurityFilterChain = 
        http.oauth2ResourceServer(keycloakOAuth2ResourceServerProvider.provideWebMvc())
            .build()
    
    // For WebFlux
    @Bean
    fun securityFilterChain(http: ServerHttpSecurity): SecurityWebFilterChain = 
        http.oauth2ResourceServer(keycloakOAuth2ResourceServerProvider.provideWebFlux())
            .build()
}
```

If you have provided a `client-secret`, you will also enable a built-in client bean. For WebMVC there is a `RestClient`, for Webflux there is a `WebClient`. These are pre-configured to authenticate with the client id/secret and will have access to whatever role that client's service account has.