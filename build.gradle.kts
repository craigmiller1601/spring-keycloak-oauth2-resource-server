import com.diffplug.gradle.spotless.SpotlessExtension
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.springframework.boot.gradle.tasks.bundling.BootJar

val projectGroup: String by project
val projectVersion: String by project

plugins {
    id("org.springframework.boot") version "3.4.1"
    id("io.spring.dependency-management") version "1.1.6"
    id("us.craigmiller160.gradle.defaults") version "1.4.2"
    id("com.diffplug.spotless") version "7.0.2"
    kotlin("jvm")
    kotlin("plugin.spring")
    `maven-publish`
}

group = projectGroup
version = projectVersion
java.sourceCompatibility = JavaVersion.VERSION_21

dependencyManagement {
    imports {
        mavenBom("com.fasterxml.jackson:jackson-bom:2.17.1")
    }
}

dependencies {
    val coroutinesVersion: String by project

    implementation("org.springframework.boot:spring-boot-starter-oauth2-resource-server")
    implementation("org.springframework.boot:spring-boot-starter-security")
    implementation("org.jetbrains.kotlin:kotlin-stdlib")

    // Dependencies needed at compile time and for tests but not propagated to consuming projects
    listOf(
        "jakarta.servlet:jakarta.servlet-api:6.1.0",
        "org.jetbrains.kotlinx:kotlinx-coroutines-core-jvm:$coroutinesVersion",
        "org.jetbrains.kotlinx:kotlinx-coroutines-reactor:$coroutinesVersion",
        "org.springframework.boot:spring-boot-starter-web",
        "org.springframework.boot:spring-boot-starter-webflux"
    )
        .forEach { dependency ->
            compileOnly(dependency)
            testImplementation(dependency)
        }

    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("org.springframework.security:spring-security-test")
    testImplementation("us.craigmiller160:testcontainers-common:2.1.2")
    testImplementation("com.fasterxml.jackson.module:jackson-module-kotlin")
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "21"
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}

configure<SpotlessExtension> {
    kotlin {
        ktfmt()
    }
}

tasks.withType<BootJar> {
    enabled = false
}
