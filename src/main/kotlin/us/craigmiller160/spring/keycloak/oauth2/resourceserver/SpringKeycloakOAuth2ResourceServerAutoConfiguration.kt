package us.craigmiller160.spring.keycloak.oauth2.resourceserver

import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan(basePackages = ["us.craigmiller160.spring.keycloak.oauth2.resourceserver"])
@ConfigurationPropertiesScan(
    basePackages = ["us.craigmiller160.spring.keycloak.oauth2.resourceserver"])
class SpringKeycloakOAuth2ResourceServerAutoConfiguration
