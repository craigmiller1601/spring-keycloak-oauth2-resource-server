package us.craigmiller160.spring.keycloak.oauth2.resourceserver.client

import java.io.IOException
import java.time.ZoneId
import java.time.ZonedDateTime
import org.springframework.http.HttpRequest
import org.springframework.http.MediaType
import org.springframework.http.client.ClientHttpRequestExecution
import org.springframework.http.client.ClientHttpRequestInterceptor
import org.springframework.http.client.ClientHttpResponse
import org.springframework.util.LinkedMultiValueMap
import org.springframework.web.client.RestClient
import us.craigmiller160.spring.keycloak.oauth2.resourceserver.config.KeycloakResourceServerConfig

class RestClientOAuth2Interceptor(private val config: KeycloakResourceServerConfig) :
    ClientHttpRequestInterceptor {
  companion object {
    private val TOKEN_CONTAINER_LOCK = Any()
  }

  private val client = RestClient.builder().build()
  @Volatile
  private var tokenContainer = TokenContainer("", ZonedDateTime.now(ZoneId.of("UTC")).minusDays(1))

  override fun intercept(
      request: HttpRequest,
      body: ByteArray,
      execution: ClientHttpRequestExecution
  ): ClientHttpResponse {
    if (tokenContainer.isExpired) {
      synchronized(TOKEN_CONTAINER_LOCK) {
        if (tokenContainer.isExpired) {
          authenticateForToken()
        }

        if (tokenContainer.isExpired) {
          throw IOException("Token is still expired even after authentication")
        }
      }
    }

    request.headers.add("Authorization", "Bearer ${tokenContainer.token}")
    return execution.execute(request, body)
  }

  private fun authenticateForToken() {
    try {
      val body =
          LinkedMultiValueMap<String, String>().apply {
            add("grant_type", "client_credentials")
            add("client_id", config.clientId)
            add("client_secret", config.clientSecret)
          }

      val token =
          client
              .post()
              .uri("${config.issuerUri}/protocol/openid-connect/token")
              .body(body)
              .contentType(MediaType.APPLICATION_FORM_URLENCODED)
              .retrieve()
              .toEntity(TokenDto::class.java)
              .body

      if (token == null) {
        throw IOException("Retrieved token is null, authentication has failed")
      }

      val expiration = ZonedDateTime.now(ZoneId.of("UTC")).plusSeconds(token.expiresIn)
      tokenContainer = TokenContainer(token.accessToken, expiration)
    } catch (ex: Exception) {
      throw IOException("Failed to retrieve access token", ex)
    }
  }
}
