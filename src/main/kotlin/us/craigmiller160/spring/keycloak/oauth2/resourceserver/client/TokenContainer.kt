package us.craigmiller160.spring.keycloak.oauth2.resourceserver.client

import java.time.ZoneId
import java.time.ZonedDateTime

data class TokenContainer(val token: String, val expiration: ZonedDateTime) {
  val isExpired: Boolean
    get() = ZonedDateTime.now(ZoneId.of("UTC")).isAfter(expiration.minusSeconds(30))
}
