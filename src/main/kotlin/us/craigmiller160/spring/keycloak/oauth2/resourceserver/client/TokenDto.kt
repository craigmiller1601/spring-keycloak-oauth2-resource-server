package us.craigmiller160.spring.keycloak.oauth2.resourceserver.client

import com.fasterxml.jackson.annotation.JsonProperty

data class TokenDto(
    @field:JsonProperty("access_token") val accessToken: String,
    @field:JsonProperty("expires_in") val expiresIn: Long,
    @field:JsonProperty("refresh_expires_in") val refreshExpiresIn: Long,
    @field:JsonProperty("refresh_token") val refreshToken: String?,
    @field:JsonProperty("token_type") val tokenType: String,
    @field:JsonProperty("id_token") val idToken: String?,
    @field:JsonProperty("not_before_policy") val notBeforePolicy: Long,
    val scope: String
)
