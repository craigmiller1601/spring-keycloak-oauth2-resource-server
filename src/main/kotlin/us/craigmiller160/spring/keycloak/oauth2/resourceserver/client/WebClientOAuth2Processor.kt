package us.craigmiller160.spring.keycloak.oauth2.resourceserver.client

import java.io.IOException
import java.time.ZoneId
import java.time.ZonedDateTime
import java.util.function.Function
import kotlinx.coroutines.reactor.awaitSingle
import kotlinx.coroutines.reactor.mono
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import org.springframework.http.MediaType
import org.springframework.util.LinkedMultiValueMap
import org.springframework.web.reactive.function.client.ClientRequest
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Mono
import us.craigmiller160.spring.keycloak.oauth2.resourceserver.config.KeycloakResourceServerConfig

class WebClientOAuth2Processor(private val config: KeycloakResourceServerConfig) :
    Function<ClientRequest, Mono<ClientRequest>> {
  private val client = WebClient.builder().build()
  /** This mutext is intended to protect access to the tokenContainer field. */
  private val tokenContainerMutext = Mutex()
  @Volatile
  private var tokenContainer = TokenContainer("", ZonedDateTime.now(ZoneId.of("UTC")).minusDays(1))

  override fun apply(request: ClientRequest): Mono<ClientRequest> = mono {
    if (tokenContainer.isExpired) {
      tokenContainerMutext.withLock {
        if (tokenContainer.isExpired) {
          authenticateForToken()
        }

        if (tokenContainer.isExpired) {
          throw IOException("Token is still expired even after authentication")
        }
      }
    }

    ClientRequest.from(request).header("Authorization", "Bearer ${tokenContainer.token}").build()
  }

  private suspend fun authenticateForToken() {
    try {
      val body =
          LinkedMultiValueMap<String, String>().apply {
            add("grant_type", "client_credentials")
            add("client_id", config.clientId)
            add("client_secret", config.clientSecret)
          }

      val token =
          client
              .post()
              .uri("${config.issuerUri}/protocol/openid-connect/token")
              .contentType(MediaType.APPLICATION_FORM_URLENCODED)
              .bodyValue(body)
              .retrieve()
              .toEntity(TokenDto::class.java)
              .awaitSingle()
              .body

      if (token == null) {
        throw IOException("Retrieved token is null, authentication has failed")
      }

      val expiration = ZonedDateTime.now(ZoneId.of("UTC")).plusSeconds(token.expiresIn)
      tokenContainer = TokenContainer(token.accessToken, expiration)
    } catch (ex: Exception) {
      throw IOException("Failed to retrieve access token", ex)
    }
  }
}
