package us.craigmiller160.spring.keycloak.oauth2.resourceserver.config

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.client.RestClient
import org.springframework.web.reactive.function.client.ExchangeFilterFunction
import org.springframework.web.reactive.function.client.WebClient
import us.craigmiller160.spring.keycloak.oauth2.resourceserver.client.RestClientOAuth2Interceptor
import us.craigmiller160.spring.keycloak.oauth2.resourceserver.client.WebClientOAuth2Processor

@Configuration
@ConditionalOnProperty(
    name = ["spring.security.keycloak.oauth2.resourceserver.client-secret"], matchIfMissing = false)
class KeycloakClientConfig {
  @Bean
  @ConditionalOnProperty(name = ["spring.main.web-application-type"], havingValue = "servlet")
  fun restClient(config: KeycloakResourceServerConfig): RestClient =
      RestClient.builder().requestInterceptor(RestClientOAuth2Interceptor(config)).build()

  @Bean
  @ConditionalOnProperty(name = ["spring.main.web-application-type"], havingValue = "reactive")
  fun webClient(config: KeycloakResourceServerConfig): WebClient =
      WebClient.builder()
          .filter(ExchangeFilterFunction.ofRequestProcessor(WebClientOAuth2Processor(config)))
          .build()
}
