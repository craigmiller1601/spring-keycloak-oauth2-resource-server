package us.craigmiller160.spring.keycloak.oauth2.resourceserver.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.core.env.Environment

@ConfigurationProperties(prefix = "spring.security.keycloak.oauth2.resourceserver")
data class KeycloakResourceServerConfig(
    val host: String,
    val realm: String,
    val clientId: String,
    val clientSecret: String?,
    val altHosts: List<String> = listOf()
) {
  companion object {
    fun fromEnvironment(environment: Environment): KeycloakResourceServerConfig {
      val altHosts =
          (environment.getProperty(
                  "spring.security.keycloak.oauth2.resourceserver.alt-issuers", List::class.java)
                  ?: listOf<String>())
              .stream()
              .map { it.toString() }
              .toList()

      return KeycloakResourceServerConfig(
          host =
              environment.getProperty("spring.security.keycloak.oauth2.resourceserver.host") ?: "",
          realm =
              environment.getProperty("spring.security.keycloak.oauth2.resourceserver.realm") ?: "",
          clientId =
              environment.getProperty("spring.security.keycloak.oauth2.resourceserver.client-id")
                  ?: "",
          clientSecret =
              environment.getProperty(
                  "spring.security.keycloak.oauth2.resourceserver.client-secret"),
          altHosts = altHosts)
    }
  }

  val issuerUri = "$host/realms/$realm"
  val altIssuerUris = altHosts.map { "$it/realms/$realm" }
  val jwkSetUri = "$issuerUri/protocol/openid-connect/certs"
  val principalAttribute = "preferred_username"
}
