package us.craigmiller160.spring.keycloak.oauth2.resourceserver.security

import org.springframework.security.oauth2.core.OAuth2Error
import org.springframework.security.oauth2.core.OAuth2ErrorCodes
import org.springframework.security.oauth2.core.OAuth2TokenValidator
import org.springframework.security.oauth2.core.OAuth2TokenValidatorResult
import org.springframework.security.oauth2.jwt.Jwt

class JwtIssuerValidator(private val issuers: List<String>) : OAuth2TokenValidator<Jwt> {
  override fun validate(token: Jwt?): OAuth2TokenValidatorResult {
    if (token == null) {
      return OAuth2TokenValidatorResult.failure(
          OAuth2Error(OAuth2ErrorCodes.INVALID_TOKEN, "The token is invalid", ""))
    }

    if (!issuers.contains(token.issuer.toString())) {
      return OAuth2TokenValidatorResult.failure(
          OAuth2Error(OAuth2ErrorCodes.INVALID_TOKEN, "The token issuer is invalid", ""))
    }

    return OAuth2TokenValidatorResult.success()
  }
}
