package us.craigmiller160.spring.keycloak.oauth2.resourceserver.security

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.annotation.Bean
import org.springframework.security.config.Customizer
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configurers.oauth2.server.resource.OAuth2ResourceServerConfigurer
import org.springframework.security.config.web.server.ServerHttpSecurity
import org.springframework.security.oauth2.core.DelegatingOAuth2TokenValidator
import org.springframework.security.oauth2.jwt.NimbusJwtDecoder
import org.springframework.security.oauth2.jwt.NimbusReactiveJwtDecoder
import org.springframework.stereotype.Component
import us.craigmiller160.spring.keycloak.oauth2.resourceserver.config.KeycloakResourceServerConfig

@Component
class KeycloakOAuth2Customizers(private val config: KeycloakResourceServerConfig) {
  @ConditionalOnProperty(name = ["spring.main.web-application-type"], havingValue = "reactive")
  @Bean("webfluxKeycloakOAuth2ResourceServer")
  fun webfluxKeycloakOAuth2ResourceServer():
      Customizer<ServerHttpSecurity.OAuth2ResourceServerSpec> =
      Customizer<ServerHttpSecurity.OAuth2ResourceServerSpec> { spec ->
        val decoder =
            NimbusReactiveJwtDecoder.withJwkSetUri(config.jwkSetUri).build().apply {
              setJwtValidator(
                  DelegatingOAuth2TokenValidator(
                      listOf(JwtIssuerValidator(listOf(config.issuerUri) + config.altIssuerUris))))
            }

        spec.jwt { jwt ->
          jwt.jwtAuthenticationConverter(WebFluxJwtAuthConverter(config)).jwtDecoder(decoder)
        }
      }

  @ConditionalOnProperty(name = ["spring.main.web-application-type"], havingValue = "servlet")
  @Bean("webmvcKeycloakOAuth2ResourceServer")
  fun webmvcKeycloakOAuth2ResourceServer():
      Customizer<OAuth2ResourceServerConfigurer<HttpSecurity>> =
      Customizer<OAuth2ResourceServerConfigurer<HttpSecurity>> { spec ->
        val decoder =
            NimbusJwtDecoder.withJwkSetUri(config.jwkSetUri).build().apply {
              setJwtValidator(
                  DelegatingOAuth2TokenValidator(
                      listOf(JwtIssuerValidator(listOf(config.issuerUri) + config.altIssuerUris))))
            }

        spec.jwt { jwt ->
          jwt.jwtAuthenticationConverter(WebMvcJwtAuthConverter(config)).decoder(decoder)
        }
      }
}
