package us.craigmiller160.spring.keycloak.oauth2.resourceserver.security

import org.springframework.core.convert.converter.Converter
import org.springframework.security.authentication.AbstractAuthenticationToken
import org.springframework.security.oauth2.jwt.Jwt
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken
import reactor.core.publisher.Mono
import us.craigmiller160.spring.keycloak.oauth2.resourceserver.config.KeycloakResourceServerConfig

class WebFluxJwtAuthConverter(private val config: KeycloakResourceServerConfig) :
    Converter<Jwt, Mono<AbstractAuthenticationToken>> {
  override fun convert(jwt: Jwt): Mono<AbstractAuthenticationToken> {
    val principal = jwt.getClaim<String>(config.principalAttribute)
    return JwtAuthenticationToken(jwt, getRoles(jwt, config.clientId), principal).let {
      Mono.just(it)
    }
  }
}
