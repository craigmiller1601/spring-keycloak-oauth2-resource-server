package us.craigmiller160.spring.keycloak.oauth2.resourceserver

val AUTH_HOST_REGEX = Regex("""http:\/\/(?<host>.+):(?<port>.+)""")

fun getAuthHostPort(hostUrl: String): String =
    AUTH_HOST_REGEX.find(hostUrl)?.groups?.get("port")?.value
        ?: throw IllegalStateException("Cannot get port from $hostUrl")
