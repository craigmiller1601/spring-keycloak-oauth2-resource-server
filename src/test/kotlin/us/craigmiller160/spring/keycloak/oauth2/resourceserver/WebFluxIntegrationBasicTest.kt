package us.craigmiller160.spring.keycloak.oauth2.resourceserver

import java.net.InetAddress
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.web.reactive.server.WebTestClient
import us.craigmiller160.springcontainer.common.DefaultUsers
import us.craigmiller160.springcontainer.webflux.WebFluxIntegrationTest
import us.craigmiller160.testcontainers.common.core.AuthenticationHelper

@WebFluxIntegrationTest
class WebFluxIntegrationBasicTest
@Autowired
constructor(
    private val webTestClient: WebTestClient,
    private val defaultUsers: DefaultUsers,
    private val helper: AuthenticationHelper
) {
  @Test
  fun `grants access for user with valid token`() {
    webTestClient
        .get()
        .uri("/hello")
        .header("Authorization", "Bearer ${defaultUsers.primaryUser.token}")
        .exchange()
        .expectStatus()
        .isOk
        .expectBody()
        .returnResult()
        .responseBody!!
        .let { assertEquals("Hello World", String(it)) }
  }

  @Test
  fun `rejects access when no token`() {
    webTestClient.get().uri("/hello").exchange().expectStatus().isUnauthorized
  }

  @Test
  fun `rejects access without role`() {
    webTestClient
        .get()
        .uri("/hello")
        .header("Authorization", "Bearer ${defaultUsers.tertiaryUser.token}")
        .exchange()
        .expectStatus()
        .isForbidden
  }

  @Test
  fun `rejects access with invalid issuer`() {
    val machineName = InetAddress.getLocalHost().hostName
    val port =
        AUTH_HOST_REGEX.find(AuthenticationHelper.containerUrl)?.groups?.get("port")?.value
            ?: throw IllegalStateException(
                "Cannot get port from ${AuthenticationHelper.containerUrl}")
    val host = "http://${machineName}:$port"
    val loggedInUser =
        helper.login(
            defaultUsers.primaryUser.toTestUser(), AuthenticationHelper.LoginOverrides(host = host))

    webTestClient
        .get()
        .uri("/hello")
        .header("Authorization", "Bearer ${loggedInUser.token}")
        .exchange()
        .expectStatus()
        .isUnauthorized
  }
}
