package us.craigmiller160.spring.keycloak.oauth2.resourceserver

import java.net.InetAddress
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.get
import us.craigmiller160.springcontainer.common.DefaultUsers
import us.craigmiller160.springcontainer.webmvc.WebMvcIntegrationTest
import us.craigmiller160.testcontainers.common.core.AuthenticationHelper

@WebMvcIntegrationTest
class WebMvcIntegrationAltIssuerTest
@Autowired
constructor(
    private val mockMvc: MockMvc,
    private val defaultUsers: DefaultUsers,
    private val helper: AuthenticationHelper
) {
  companion object {
    @DynamicPropertySource
    @JvmStatic
    fun registerProperties(registry: DynamicPropertyRegistry) {
      val existingHost = System.getProperty("testcontainers.common.keycloak.url")
      val port = getAuthHostPort(existingHost)
      registry.add("spring.security.keycloak.oauth2.resourceserver.alt-hosts") {
        val host = "http://${InetAddress.getLocalHost().hostName}:$port"
        println("Host being configured as an allowed alternate host: $host")
        listOf(host)
      }
    }
  }

  @Test
  fun `allows access with alternate issuer`() {
    val machineName = InetAddress.getLocalHost().hostName
    val port = getAuthHostPort(AuthenticationHelper.containerUrl)
    val host = "http://${machineName}:$port"
    println("Host being used to issue token: $host")
    val loggedInUser =
        helper.login(
            defaultUsers.primaryUser.toTestUser(), AuthenticationHelper.LoginOverrides(host = host))

    mockMvc
        .get("/hello") { header("Authorization", "Bearer ${loggedInUser.token}") }
        .andExpect {
          status { isOk() }
          content { string("Hello World") }
        }
  }
}
