package us.craigmiller160.spring.keycloak.oauth2.resourceserver

import java.net.InetAddress
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.get
import us.craigmiller160.springcontainer.common.DefaultUsers
import us.craigmiller160.springcontainer.webmvc.WebMvcIntegrationTest
import us.craigmiller160.testcontainers.common.core.AuthenticationHelper

@WebMvcIntegrationTest
class WebMvcIntegrationBasicTest
@Autowired
constructor(
    private val mockMvc: MockMvc,
    private val defaultUsers: DefaultUsers,
    private val helper: AuthenticationHelper
) {
  @Test
  fun `grants access for user with valid token`() {
    mockMvc
        .get("/hello") { header("Authorization", "Bearer ${defaultUsers.primaryUser.token}") }
        .andExpect {
          status { isOk() }
          content { string("Hello World") }
        }
  }

  @Test
  fun `rejects access when no token`() {
    mockMvc.get("/hello").andExpect { status { isUnauthorized() } }
  }

  @Test
  fun `rejects access without role`() {
    mockMvc
        .get("/hello") { header("Authorization", "Bearer ${defaultUsers.tertiaryUser.token}") }
        .andExpect { status { isForbidden() } }
  }

  @Test
  fun `rejects access with invalid issuer`() {
    val machineName = InetAddress.getLocalHost().hostName
    val port =
        AUTH_HOST_REGEX.find(AuthenticationHelper.containerUrl)?.groups?.get("port")?.value
            ?: throw IllegalStateException(
                "Cannot get port from ${AuthenticationHelper.containerUrl}")
    val host = "http://${machineName}:$port"
    val loggedInUser =
        helper.login(
            defaultUsers.primaryUser.toTestUser(), AuthenticationHelper.LoginOverrides(host = host))

    mockMvc
        .get("/hello") { header("Authorization", "Bearer ${loggedInUser.token}") }
        .andExpect { status { isUnauthorized() } }
  }
}
