package us.craigmiller160.spring.keycloak.oauth2.resourceserver.client

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.web.server.LocalServerPort
import org.springframework.http.HttpStatus
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.springframework.web.reactive.function.client.WebClient
import us.craigmiller160.springcontainer.webflux.WebFluxIntegrationTest

@WebFluxIntegrationTest
class WebFluxClientIntegrationTest
@Autowired
constructor(private val webClient: WebClient, @LocalServerPort private val port: Int) {
  companion object {
    @DynamicPropertySource
    @JvmStatic
    fun registerProperties(registry: DynamicPropertyRegistry) {
      registry.add("spring.security.keycloak.oauth2.resourceserver.client-secret") {
        "\${testcontainers.common.keycloak.client.secret}"
      }
    }
  }

  @Test
  fun `it automatically authenticates prior to making call`() {
    val entity =
        webClient
            .get()
            .uri("http://localhost:$port/hello")
            .retrieve()
            .toEntity(String::class.java)
            .block()
    assertThat(entity!!.statusCode).isEqualTo(HttpStatus.OK)
    assertThat(entity!!.body).isNotEmpty.isEqualTo("Hello World")
  }
}
