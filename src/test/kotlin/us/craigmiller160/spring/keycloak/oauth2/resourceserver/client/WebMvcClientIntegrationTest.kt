package us.craigmiller160.spring.keycloak.oauth2.resourceserver.client

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.web.server.LocalServerPort
import org.springframework.http.HttpStatus
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.springframework.web.client.RestClient
import us.craigmiller160.springcontainer.webmvc.WebMvcIntegrationTest

@WebMvcIntegrationTest
class WebMvcClientIntegrationTest
@Autowired
constructor(private val restClient: RestClient, @LocalServerPort private val port: Int) {
  companion object {
    @DynamicPropertySource
    @JvmStatic
    fun registerProperties(registry: DynamicPropertyRegistry) {
      registry.add("spring.security.keycloak.oauth2.resourceserver.client-secret") {
        "\${testcontainers.common.keycloak.client.secret}"
      }
    }
  }

  @Test
  fun `it automatically authenticates prior to making call`() {
    val entity =
        restClient.get().uri("http://localhost:$port/hello").retrieve().toEntity(String::class.java)
    assertThat(entity.statusCode).isEqualTo(HttpStatus.OK)
    assertThat(entity.body).isNotEmpty.isEqualTo("Hello World")
  }
}
