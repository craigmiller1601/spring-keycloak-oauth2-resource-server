package us.craigmiller160.springcontainer.common

import us.craigmiller160.testcontainers.common.core.AuthenticationHelper

data class DefaultUsers(
    val primaryUser: AuthenticationHelper.TestUserWithToken,
    val secondaryUser: AuthenticationHelper.TestUserWithToken,
    val tertiaryUser: AuthenticationHelper.TestUserWithToken
)
