package us.craigmiller160.springcontainer.webflux

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity
import org.springframework.security.config.web.server.ServerHttpSecurity
import org.springframework.security.web.server.SecurityWebFilterChain
import us.craigmiller160.spring.keycloak.oauth2.resourceserver.security.KeycloakOAuth2ResourceServerProvider

@Configuration
@EnableWebFluxSecurity
class SecurityConfig(
    private val keycloakOAuth2ResourceServerProvider: KeycloakOAuth2ResourceServerProvider
) {
  @Bean
  fun securityFilterChain(http: ServerHttpSecurity): SecurityWebFilterChain =
      http
          .csrf { it.disable() }
          .oauth2ResourceServer(keycloakOAuth2ResourceServerProvider.provideWebFlux())
          .authorizeExchange { it.anyExchange().hasAnyRole("access") }
          .build()
}
