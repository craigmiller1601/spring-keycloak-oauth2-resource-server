package us.craigmiller160.springcontainer.webflux

import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan(
    basePackages =
        [
            "us.craigmiller160.springcontainer.webflux",
            "us.craigmiller160.springcontainer.common",
            "us.craigmiller160.spring.keycloak.oauth2.resourceserver"])
@EnableAutoConfiguration
class WebFluxConfig
