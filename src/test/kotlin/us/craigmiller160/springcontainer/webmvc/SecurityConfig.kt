package us.craigmiller160.springcontainer.webmvc

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.web.SecurityFilterChain
import us.craigmiller160.spring.keycloak.oauth2.resourceserver.security.KeycloakOAuth2ResourceServerProvider

@Configuration
@EnableWebSecurity
class SecurityConfig(
    private val keycloakOAuth2ResourceServerProvider: KeycloakOAuth2ResourceServerProvider
) {
  @Bean
  fun securityFilterChain(http: HttpSecurity): SecurityFilterChain =
      http
          .csrf { it.disable() }
          .oauth2ResourceServer(keycloakOAuth2ResourceServerProvider.provideWebMvc())
          .authorizeHttpRequests { it.anyRequest().hasAnyRole("access") }
          .build()
}
