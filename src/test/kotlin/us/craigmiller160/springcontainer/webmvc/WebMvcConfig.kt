package us.craigmiller160.springcontainer.webmvc

import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan(
    basePackages =
        [
            "us.craigmiller160.springcontainer.webmvc",
            "us.craigmiller160.springcontainer.common",
            "us.craigmiller160.spring.keycloak.oauth2.resourceserver"])
class WebMvcConfig
