package us.craigmiller160.springcontainer.webmvc

import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit.jupiter.SpringExtension
import us.craigmiller160.testcontainers.common.TestcontainersExtension

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
@ExtendWith(TestcontainersExtension::class, SpringExtension::class)
@SpringBootTest(
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
    classes = [WebMvcConfig::class],
    properties =
        [
            "spring.main.web-application-type=servlet",
            "spring.security.keycloak.oauth2.resourceserver.host=\${testcontainers.common.keycloak.url}",
            "spring.security.keycloak.oauth2.resourceserver.realm=\${testcontainers.common.keycloak.realm}",
            "spring.security.keycloak.oauth2.resourceserver.client-id=\${testcontainers.common.keycloak.client.id}"])
@AutoConfigureMockMvc
@ActiveProfiles("test")
@EnableAutoConfiguration
annotation class WebMvcIntegrationTest
